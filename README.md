# Node Rest API

This API Review Using Node.Js

## Requirements
*  Node JS
*  Mongo
*  Package Management Like Yarn or NPM

## Installation

*  Clone This Repository
*  Install dependencies (i'm using yarn):
`yarn install`

## Running
*  if you lazy to input data use this inject:
`yarn inject`
*  running it
`yarn runserver`

## Want to Try

I've already deployed and this Mycolecction :

`https://www.getpostman.com/collections/e0ac5dc63ca388dd8070`