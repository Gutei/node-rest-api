var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Review = require('./models/review'), //created model loading here
  bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
  
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/review', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

require('./routes/review')(app);//register the route

app.listen(port);

console.log('Server started on: ' + port);
