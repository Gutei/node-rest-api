const Review = require('../models/review');

exports.list_review = function(req, res) {
    Review.find({}, function(err, review) {
    if (err)
      res.send(err);
    res.json(review);
  });
};

exports.create_review= function(req, res) {
  var data = req.body;

  var new_review = new Review(req.body);
  new_review.save(function(err, review) {
    if (err)
      res.send(err);
    res.json(review);
  });
};


exports.get_review = function(req, res) {
  Review.findById(req.params.id, function(err, review) {
    if (err)
      res.send(err);
    res.json(review);
  });
};


exports.update_review = function(req, res) {
  Review.updateOne({_id: req.params.id}, req.body, {new: true}, function(err, review) {
    if (review.n == 0)
        return res.status(400).send({
            message: "Review not found"
        });
    if (err)
        return res.status(500).send(err);
    if (review.nModified == 1)
        return res.status(200).send({
            message: "Update Successfull"
        });
  });
};

exports.delete_review = function(req, res) {
  Review.deleteOne({_id: req.params.id}, function(err, review) {
    if (review.n == 0)
        return res.status(400).send({
            message: "Review not found"
        });
    if (err)
        return res.status(500).send(err);
    if (review.deletedCount == 1)
        return res.status(200).send({
            message: "Delete Successfull"
        });
  });
};