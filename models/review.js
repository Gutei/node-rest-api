const mongoose = require('mongoose');
const Review = mongoose.Schema;

const modelReview = new Review({
    user: {
      type: String    
    },
    textReview: {
        type: String    
      },
    gambar: {
        type: String    
      },
    waktu: {
      type: Date,
      default: Date.now
    },
    jmlBintang: {
        type: Number,
        min: 0, max: 5,
        default: 0
    }
  });
  module.exports = mongoose.model('review', modelReview);