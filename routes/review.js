module.exports = (app) => {
  var review = require('../controllers/review');

  app.route('/review')
    .get(review.list_review)
    .post(review.create_review);


  app.route('/review/:id')
    .get(review.get_review)
    .put(review.update_review)
    .delete(review.delete_review);
};