const mongoose = require('mongoose');
const Review = require('../models/review');

 
// make a connection
mongoose.connect('mongodb://localhost/review', {
  useNewUrlParser: true,
  useUnifiedTopology: true
}); 
// get reference to database
const connect = mongoose.connection;
 
connect.on('error', console.error.bind(console, 'connection error:'));
 
connect.once('open', function() {
    console.log("Connection Successful!");
 
    const axios = require('axios');
    
    const url = 'https://dry-fortress-35321.herokuapp.com/reviews/getall'
    axios.get(url)
    .then(response => {
        Review.collection.insertMany(response.data, function (err, result) {
        if (err){ 
            return console.error(err);
        } else {
            console.log("Inject Successfull");
        }
    });
    })
    .catch(error => {
      console.log(error);
    });
    
}); 